<?php

namespace Boldface\SoftTransients;

defined( 'SOFTTRANSIENTS' ) or die();

/**
 * Class for loading the plugin
 *
 * @package Boldface\SoftTransients
 */
class plugin {

  /**
   * @var string Plugin basename
   *
   * @access private
   * @since 0.1
   */
  private $file;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = is_string( $file ) ? plugin_basename( $file ) : '';
  }

  /**
   * Method needs to be fired before init.
   * Add several actions to load the plugin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on initiation to load plugin text domain
    \add_action( 'init', [ $this, 'load_textdomain' ] );

    //* Register the expired transients API
    \add_action( 'plugins_loaded', [ new expired_transients(), 'register' ], 13 );
  }

  /**
   * Load the text domain
   *
   * @access public
   * @since 0.1
   */
  public function load_textdomain() {
    \load_plugin_textdomain( 'boldface-soft-transients', false, dirname( \plugin_basename( __FILE__ ) ) . '/languages' );
  }
}

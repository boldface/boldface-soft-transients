<?php

namespace Boldface\SoftTransients;

defined( 'SOFTTRANSIENTS' ) or die();

/**
 * Class for interacting with expried WordPress transients
 *
 * @package Boldface\SoftTransients
 */
class expired_transients {

  /**
  * @var $name string The name of the transient
  *
  * @access protected
  * @since 0.1
  */
  protected $name = 'boldface_expired_transients';

  /**
   * @var $transient Instance of \Boldface\BetterTransients\transient
   *
   * @access protected
   * @since 0.1
   */
  protected $transient;

  /**
   * @var $timeout
   *
   * @access protected
   * @since 0.1
   */
  protected $timeout;

  /**
   * @var Array The expired transients
   *
   * @access protected
   * @since 0.1
   */
  protected $expired_transients;

  /**
   * @var bool Whether an external object cache is in use
   *
   * @access protected
   * @since 0.1
   */
  protected $using_external_object_cache;

  /**
   * @var bool Whether the Boldface Better Transients plugin is active
   *
   * @access protected
   * @since 0.1
   */
  protected $using_boldface_better_transients;

  /**
   * Constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    $this->using_external_object_cache = \wp_using_ext_object_cache();
  }

  /**
   * Register actions to interact with the transients
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Maybe use Boldface Better Transients
    \add_action( 'plugins_loaded', [ $this, 'maybe_use_boldface_transients' ], 15 );

    //* Set the timeout
    \add_action( 'init', [ $this, 'set_timeout' ], 50 );

    //* Get all the expired transients
    \add_action( 'init', [ $this, 'get_expired_transients' ], 75 );

    //* Do something with the expired transients
    \add_action( 'init', [ $this, 'add_filters' ], 98 );
  }

  /**
   * Maybe use the Boldface Better Transients plugin
   *
   * @access public
   * @since 0.1
   */
  public function maybe_use_boldface_transients() {
    if( $this->using_boldface_better_transients =
      $this->is_boldface_better_transients_active() ) {
        $this->transient = new \Boldface\BetterTransients\transient( $this->name );
      }
  }

  /**
   * Set the timeout on the soft-transients transient
   *
   * @access public
   * @since 0.1
   */
  public function set_timeout() {
    $this->timeout = \apply_filters( '\Boldface\SoftTransients\timeout', HOUR_IN_SECONDS );
  }

  /**
   * Add pre_transient_$transient filters to all the expired transients
   *
   * @access public
   * @since 0.1
   */
  public function add_filters() {
    //* Return early if $expired_transients is not an array with more than
    //* one element
    if( ! is_array( $this->expired_transients ) || count( $this->expired_transients ) < 1 ) {
      return;
    }

    $expired_transients = \apply_filters(
      'Boldface\SoftTransients\expired_transients', $this->expired_transients );

    //* For the expired transients, add a 'pre_transient_' filter
    for( $i = 0; $i <= count( $expired_transients ) - 1; $i++ ) {
      $transient = str_replace( [ '_site_transient_timeout_', '_transient_timeout_' ], '', $this->expired_transients[ $i ] );
      \add_filter( 'pre_transient_' . $transient, [ $this, 'pre_transient' ], -1, 2 );
      \add_filter( 'pre_site_transient_' . $transient, [ $this, 'pre_transient' ], -1, 2 );
    }
  }

  /**
   * Get all the expired transients
   *
   * @access public
   * @since 0.1
   *
   * @return array An array of the expired transients
   */
  public function get_expired_transients() {
    //* Check if the soft-transients transient is set
    //* Get it and set it, if it's not already
    if( false === ( $expired_transients = $this->get_expired_transients_transient() ) ) {
      $expired_transients = $this->get_the_expired_transients();
      $this->set_expired_transients_transient( $expired_transients );
    }
    return $this->expired_transients = $expired_transients;
  }

  /**
   * Get the expired transients transient
   *
   * @access protected
   * @since 0.1
   *
   * @return mixed Array of expired transients
   */
  protected function get_expired_transients_transient() {
    if( $this->using_boldface_better_transients ) {
      return $this->transient->background( false )->get();
    } else {
      return \get_transient( $this->name );
    }
  }

  /**
   * Set the expired transients transient
   *
   * @param mixed Array of expired transients
   *
   * @access protected
   * @since 0.1
   */
  protected function set_expired_transients_transient( $expired_transients ) {
    if( $this->using_boldface_better_transients ) {
      $this->transient
        ->background( true )
        ->lock( true )
        ->expires( $this->timeout )
        ->set( $expired_transients );
    } else {
      \set_transient( $this->name, $expired_transients, $this->timeout );
    }
  }

  /**
   * Get the expired transients
   *
   * @access protected
   * @since 0.1
   *
   * @return array An array of expired transients
   */
  protected function get_the_expired_transients() {
    return $this->using_external_object_cache ? [] :
      $this->get_database_expired_transients();
  }

  /**
   * Get all expired transients from the database
   *
   * @access protected
   * @since 0.1
   *
   * @return array An array of expired transients
   */
  protected function get_database_expired_transients() {
    global $wpdb;
    $time_now = time() + $this->timeout;

    $expired_transients = $wpdb->get_col( "
      SELECT option_name
      FROM $wpdb->options
      WHERE option_name
      LIKE '%_transient_timeout_%'
      AND option_value+0 < $time_now
    " );

    return $this->expired_transients = $expired_transients;
  }

  /**
   * Prior to getting the expired transient, access the database and manually
   * retrieve the transient. Otherwise, the transient will be deleted before
   * it is retrieved and have to be regenerated. Delete the transient so it's
   * regenerated for the next user.
   *
   * @param $default   Default value of the transient.
   * @param $transient The transient name
   *
   * @access public
   * @since 0.1
   */
  public function pre_transient( $default, $transient ) {
    if( $this->using_boldface_better_transients ) {
      $new = new \Boldface\BetterTransients\transient( $transient );
      $value = $new->get_direct();
      $new->site(false)->background( true )->sudo( true )->delete();
      $new->site(true)->background( true )->sudo( true )->delete();
      return $value;
    }

    $value = \get_option( '_transient_' . $transient );
    if( false !== $value ) {
      \delete_transient( $transient );
      \delete_transient( 'lock_' . md5( $transient ) );
      return $value;
    }

    $value = \get_option( '_site_transient_' . $transient );
    \delete_site_transient( $transient );
    \delete_site_transient( 'lock_' . md5( $transient ) );
    return $value;
  }

  /**
   * Return whether the Boldface Better Transients plugin is active
   *
   * @access public
   * @since 0.1
   *
   * @return Whether the Boldface Better Transients plugin is active
   */
  protected function is_boldface_better_transients_active() {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    return \is_plugin_active( 'boldface-better-transients/boldface-better-transients.php' );
  }
}

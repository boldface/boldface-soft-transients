<?php
/**
 * Plugin Name: Boldface Soft Transients
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-soft-transients/
 * Description: Soft delete transients.
 * Version: 0.1
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI:
 * Domain Path: /languages
 * Text Domain: boldface-soft-transients
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

//* Define main plugin file
define( 'SOFTTRANSIENTS', __FILE__ );

//* Start bootstraping the plugin
require( dirname( SOFTTRANSIENTS ) . '/includes/bootstrap.php' );
add_action( 'plugins_loaded', array( $bootstrap = new soft_transients_bootstrap( SOFTTRANSIENTS ), 'register' ) );

//* Register activation and deactivation hooks
register_activation_hook( SOFTTRANSIENTS , array( $bootstrap, 'activation' ) );
register_deactivation_hook( SOFTTRANSIENTS , array( $bootstrap, 'deactivation' ) );

=== Soft Transients ===
Contributors: nathanatmoz
Tags: transients
Requires at least: 4.7
Tested up to: 4.7
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Soft delete transients.

== Description ==
For expired transients, return the value before deleting the transient.

== Installation ==
1. Upload the plugin files to the plugins directory, or install through the WordPress plugins screen.
1. Activate the plugin.

== Changelog ==

= 0.1 =
* Initial release
